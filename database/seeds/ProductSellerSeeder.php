<?php

use Illuminate\Database\Seeder;

class ProductSellerSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        factory(App\ProductSeller::class, 50)->create();
    }
}
