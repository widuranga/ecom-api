<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(ProductSeeder::class);
        $this->call(SellerSeeder::class);
        $this->call(ProductSellerSeeder::class);
        $this->call(UserSeeder::class);
    }
}
