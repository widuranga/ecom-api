<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\ProductSeller;
use Faker\Generator as Faker;


/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(ProductSeller::class, function (Faker $faker) {
    return [
        'seller_id' => $faker->numberBetween(1, 10),
        'product_id' => $faker->numberBetween(1, 50),
        'seller_price' => $faker->numberBetween(10000, 100000),
        'stock' => $faker->numberBetween(10, 100)
    ];
});
