

## Ecome API Configuration


- First run the composer update command.
- Then create a databans and setup env file.
- After that run php artisan migrate:refresh --seed.
- Run the php artisan passport:install
  And copy the 2nd key into the postman environment. You'll also need an email from the users table.
- Get the access token
