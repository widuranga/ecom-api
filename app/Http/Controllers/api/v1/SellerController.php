<?php

namespace App\Http\Controllers\api\v1;

use App\Seller;
use App\Http\Controllers\Controller;
use App\Http\Resources\SellerResource;
use App\Http\Resources\ProductResource;

/**
 * @group Seller API
 * Class SellerController
 * @package App\Http\Controllers\api\v1
 * API for the seller details
 */
class SellerController extends Controller
{
    /**
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     *All sellers list
     * @response {
     * "data": [{
        "id": 8,
        "name": "Isabel Gibson III",
        "phone": "+4131586273377",
        "address": "3793 Elta Orchard Suite 530\nPort Jaymeberg, ID 47836"
        },
        {
        "id": 9,
        "name": "Miss Jodie Wyman",
        "phone": "+7312060809624",
        "address": "352 Stiedemann Creek\nPort Brodyberg, DE 70126-4252"
        }]
     * }
     */
    public function index()
    {
        return SellerResource::collection(Seller::latest()->paginate());
    }

    /**
     * @param Seller $seller
     * @return SellerResource
     * Seller details
     * @response {
        {
            "data": {
            "id": 5,
            "name": "Lowell Feeney",
            "phone": "+5275504726485",
            "address": "30644 Alberta Fords Apt. 775\nWest Patricia, MT 26884"
            }
        }
     *
     */
    public function show(Seller $seller)
    {
        return new SellerResource($seller);
    }


    /**
     * @param Seller $seller
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     * Get product list by seller
     *
     * @response {
     *"data": [
    {
    "id": 47,
    "name": "Non pariatur architecto eos omnis.",
    "price": 62949,
    "stock_level": null,
    "description": "Eius rem eius molestias sapiente consequuntur. Omnis amet quo amet rerum animi sit ut itaque. Ut ipsum accusantium nobis sit.",
    "image": "https://source.unsplash.com/random"
    },
    {
    "id": 39,
    "name": "Dolor consequatur impedit facilis est adipisci repudiandae enim.",
    "price": 83310,
    "stock_level": null,
    "description": "Illum doloribus et totam non voluptas. Maxime velit aut sit et harum nihil corrupti. Velit voluptatem consequatur et facere asperiores eaque. Et expedita nihil soluta assumenda nemo nam. Saepe qui rerum blanditiis magni.",
    "image": "https://source.unsplash.com/random"
    }]
     * }
     */
    public function products(Seller $seller)
    {
        return ProductResource::collection($seller->products()->latest()->paginate());
    }
}
