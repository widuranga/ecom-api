<?php

namespace App\Http\Controllers\api\v1;

use App\Product;
use App\Http\Controllers\Controller;
use App\Http\Resources\SellerResource;
use App\Http\Resources\ProductResource;
use App\Http\Resources\ProductCollectionResource;

/**
 * @group Product API
 * Class ProductController
 * @package App\Http\Controllers\api\v1
 * API for the product details
 */
class ProductController extends Controller
{
    /**
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     *
     * Get All Product
     *
     * @response {
     *"data": [{
        "id": 45,
        "name": "Dolores illo suscipit beatae vel.",
        "price": 99268,
        "stock_level": null,
        "description": "Nihil quia voluptatem saepe nihil nulla. Ad harum occaecati eaque cupiditate. Officia doloremque minus velit voluptas culpa.",
        "image": "https://source.unsplash.com/random"
        },
        {
        "id": 46,
        "name": "Rem nobis voluptatem magni ut perspiciatis saepe in dolore.",
        "price": 61632,
        "stock_level": null,
        "description": "Sint quam repellendus non odit. Eos dolores corrupti tempore voluptas qui quam quod. Nisi ducimus doloribus odit enim occaecati omnis et doloremque.",
        "image": "https://source.unsplash.com/random"
        }]
     * }
     */
    public function index()
    {
        return ProductCollectionResource::collection(Product::latest()->paginate());
    }

    /**
     * @param Product $product
     * @return ProductResource
     *
     * Get details of a product
     *
     * @response {
     *"data": {
            "id": 1,
            "name": "Ut velit vel dignissimos aut pariatur sed ut.",
            "price": 91135,
            "stock_level": null,
            "description": "Ea pariatur sed placeat iure quidem veritatis. Dolores suscipit ea voluptate earum ipsum. Quae dolorem ipsum quasi accusamus minus sed. Cumque corporis eaque aliquid quasi at qui ut sed. Placeat consequatur perspiciatis repudiandae aut unde aut necessitatibus.",
            "image": "https://source.unsplash.com/random"
            }
     * }
     */
    public function show(Product $product)
    {
        return new ProductCollectionResource($product);
    }

    /**
     * @param Product $product
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     *
     * Get seller details for a product
     *
     * @response {
     *"data": [
            {
            "id": 6,
            "name": "Antonette Dietrich",
            "phone": "+1113979006065",
            "address": "71025 Manuela Grove Suite 367\nSouth Jeramieside, AZ 46202-2468"
            }
            ]
     * }
     */
    public function seller(Product $product)
    {
        return SellerResource::collection($product->sellers()->latest()->paginate());
    }
}
