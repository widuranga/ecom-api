<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'description' => $this->description,
            'image' => $this->image,
            'price' => $this->whenPivotLoaded('product_seller', function () {
                return $this->pivot->seller_price;
            }),
            'seller' => $this->whenPivotLoaded('product_seller', function () {
                return $this->pivot->seller_id;
            }),
            'stock' => $this->whenPivotLoaded('product_seller', function () {
                return $this->pivot->stock;
            }),
        ];
    }
}
