<?php

namespace App;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'name',
        'image',
        'description',
        'price'
    ];

    public function sellers()
    {
        return $this->belongsToMany(Seller::class)
            ->using(ProductSeller::class)
            ->withPivot([
                'seller_price',
                'stock',
            ]);
    }
    protected $appends = array('stock');
    public function getStockAttribute()
    {
        return $this->sellers()->sum('stock');
    }
}
