<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Seller extends Model
{
    public function products()
    {
        return $this->belongsToMany(Product::class)
            ->using(ProductSeller::class)
            ->withPivot([
                'seller_price',
                'stock',
            ]);
    }
}
